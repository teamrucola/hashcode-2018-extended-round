﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ConsoleApp1
{
    public class Corsa
    {
        public int id { get; set; }

        public Point partenza { get; set; }
        public Point arrivo { get; set; }

        public int tempoPartenzaMin { get; set; }
        public int tempoPartenzaVero { get; set; }
        
        public int tempoArrivoMax { get; set; }
        public int tempoArrivoVero { get; set; }

        public int distanza { get; set; }

        public int deadline { get; set; }

        public Corsa(int id, Point partenza, Point arrivo, int tempoPartenzaMin, int tempoArrivoMax)
        {
            this.id = id;
            this.partenza = partenza;
            this.arrivo = arrivo;
            this.tempoPartenzaMin = tempoPartenzaMin;
            this.tempoArrivoMax = tempoArrivoMax;
            this.distanza = Math.Abs(arrivo.Y - partenza.Y) + Math.Abs(arrivo.X - partenza.X);
            this.deadline = tempoArrivoMax - distanza;
        }
        
        public Corsa(Corsa original) {
            this.id = original.id;
            this.partenza = original.partenza;
            this.arrivo = original.arrivo;
            this.tempoPartenzaMin = original.tempoPartenzaMin;
            this.tempoArrivoMax = original.tempoArrivoMax;
            this.distanza = original.distanza;
            this.deadline = original.deadline;
            this.tempoPartenzaVero = original.tempoPartenzaVero;
            this.tempoArrivoVero = original.tempoArrivoVero;
        }

        public int DistanzaArrivoDa(Point p ) => Math.Abs(p.Y - arrivo.Y) + Math.Abs(p.X - arrivo.X);
        public int DistanzaPartenzaDa(Point p)=>Math.Abs(p.Y - partenza.Y) + Math.Abs(p.X - partenza.X);
        
    }
}
