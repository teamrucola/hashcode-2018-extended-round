﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ConsoleApp1
{
    public class Taxi
    {
        public int Id { get; private set; }
        List<Corsa> corse = new List<Corsa>();
        public int Bonus { get; private set; }
        public int Score { get; private set; }

        public int DistanzaPercorsa { get; private set; }

        //Last
        public Corsa last { get; private set; }

        public Taxi(int id,int bonus)
        {
            Id = id;
            Bonus = bonus;
            corse.Add(new Corsa(-1, new System.Drawing.Point(0, 0), new System.Drawing.Point(0, 0), 0, 0));
            last = corse.FindLast(x=>x.id==-1);
        }

        public Taxi(Taxi original) {
            this.Id = original.Id;
            this.Bonus = original.Bonus;
            original.corse.ForEach(c => this.corse.Add(new Corsa(c)));
            this.Score = original.Score;
        }

        public Point CurrentPosition() => last.arrivo;

        public void AddCorsa(Corsa c)
        {
            c.tempoPartenzaVero = BonusDelay(c) >= 0 ? c.tempoPartenzaMin : c.tempoPartenzaMin + Math.Abs(BonusDelay(c));
            c.tempoArrivoVero = c.tempoPartenzaVero + c.distanza;
            Score += BonusDelay(c) >= 0 ? Bonus + c.distanza : IsFeasable(c)?c.distanza:0;
            DistanzaPercorsa += c.distanza;
            corse.Add(c);
            last = c;
        }
        public int PotentialScore(Corsa c) => BonusDelay(c) >= 0 ? Bonus + c.distanza : IsFeasable(c) ? c.distanza : 0;

        public float ProbabilitaBonus(Corsa c)
        {
            //Penalizzo di poco le run che mi danno bonus ma che devo aspettare per iniziare
            //return Math.Max(BonusDelay(c), 0);
            float x = BonusDelay(c);
            return x >= 0 ? x : 1/2;
            //return x >= 0 ? x : (-x/(1-x));
        }

        private int BonusDelay(Corsa c) 
        {
            return c.tempoPartenzaMin - last.tempoArrivoVero - DistanzaVerso(c);
        }

        public bool IsFeasable(Corsa c)
        {
            return c.tempoArrivoMax - last.tempoArrivoVero - DistanzaVerso(c) - c.distanza >= 0;
        }
        public int Feasibility(Corsa c) => c.tempoArrivoMax - last.tempoArrivoVero - DistanzaVerso(c) - c.distanza;
        public int DistanzaVerso(Corsa c)
        {
            return Math.Abs(c.partenza.Y - last.arrivo.Y) + Math.Abs(c.partenza.X - last.arrivo.X);
        }
        public void Reset()
        {
            corse.Clear();
            corse.Add(new Corsa(-1, new System.Drawing.Point(0, 0), new System.Drawing.Point(0, 0), 0, 0));
            last = corse.FindLast(x => x.id == -1);
            Score = 0;
            DistanzaPercorsa = 0;
        }
        public String ToString()
        {
            List<Corsa> toProcess = new List<Corsa>(corse);
            toProcess.Remove(toProcess.First());
            String s = String.Format("{0}", toProcess.Count);
            toProcess.ForEach(toP => s += String.Format(" {0}", toP.id));
            return s;
        }
    }
}
